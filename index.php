<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>projectTWO</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
    <script src="js/main.js"></script>
</head>

<body>
    <header>
        <div class="container">
            <img src="http://www.starbucks.nl/media/logo_tcm15-366_w1024_n.png" alt="">
            <h1>Starbucks Coffee Enschede</h1>
        </div>
    </header>
    
    <?php
    // connectie maken database server
    $conn = new PDO('mysql:host=127.0.0.1:8889;dbname=projectTWO', 'root', 'root');

    $stmtSUM = $conn->prepare("SELECT SUM(aantalkopjes) AS value_sum FROM deelnemers");
    $stmtAVG = $conn->prepare("SELECT AVG(aantalkopjes) AS value_avg FROM deelnemers");
    $stmtMIN = $conn->prepare("SELECT MIN(aantalkopjes) AS value_min FROM deelnemers");
    $stmtMAX = $conn->prepare("SELECT MAX(aantalkopjes) AS value_max FROM deelnemers");
    $stmtNameMax = $conn->prepare("SELECT * FROM deelnemers WHERE aantalkopjes =(SELECT MAX(aantalkopjes) AS value_max FROM deelnemers))");
    //$sql = "select naam, aantalkopjes from deelnemers where aantalkopjes = (select max(aantalkopjes) from deelnemers)";


    $stmtSUM->execute();
    $stmtAVG->execute();
    $stmtMIN->execute();
    $stmtMAX->execute();
    $stmtNameMax->execute();

    $row = $stmtSUM->fetch();
    $sum = $row['value_sum'];

    $row = $stmtAVG->fetch();
    $avg = $row['value_avg'];

    $row = $stmtMIN->fetch();
    $min = $row['value_min'];
    
    $row = $stmtMAX->fetch();
    $max = $row['value_max'];

    $row = $stmtNameMax->fetch();
    $Name = $row['naam'];
    $aantalKopjes = $row['aantalkopjes'];
    
    
   echo 'Aantal kopjes :'.$sum;
   echo 'Gemiddeld kopjes :'.$avg;
   echo 'min kopjes :'.$min;
   echo 'max kopjes :'.$max;
   echo 'max kopjes + naam :'.$Name . ' ' . $aantalKopjes;
// var_dump(print_r('<pre>'.print_r($row,true).'</pre>'));
        
    ?>

    <div class="container">
        <div class="coffeelist">
            <h2>Deelnemer toevoegen</h2>
            <form action="dbinsert.php" method="POST">
                <input type="text" name="naam" placeholder="Naam...">
                <input type="text" name="woonplaats" placeholder="Woonplaats...">
                <input type="number" name="aantalkopjes" placeholder="Aantal kopjes per dag...">
                <button type="submit">Voeg nieuwe deelnemer toe</button>
            </form>
            <hr>
            <h2><span class="seagreen">Starbucks Coffee</span> deelnemers overzicht</h2>
            <?php

            // stuur SQL QUERY naar de database server
            //$stmt = $conn->query('SELECT * FROM deelnemers');
            // $stmt = $conn->query('SELECT * FROM deelnemers WHERE aantalkopjes >= 1');
            $stmt = $conn->prepare('SELECT * FROM deelnemers');
            $stmt->execute();

            // antwoord van databse server opvragen
            // door het antwoord lopen,
            while ($row = $stmt->fetch()) {
                //en het weergeven als pagina naar gebruiker als HTML
                echo '<li>' . $row['naam'] . ' komt uit ' . $row['woonplaats'] . ' en drinkt ' . '<span class="aantalkopjes">' . $row['aantalkopjes'] . '</span>' . ' kopjes koffie per dag.' . '</li>';
            }

            // verbreek de verbinding met de database server
            $conn = NULL;

            ?>
        </div>
    </div>

</body>

</html>