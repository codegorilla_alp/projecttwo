<?php
 class Car {
    public $brand;
    public $model;
    public $doors;
    public $year;
    public $price;
    public $gas;
    public $mileage;
    public $bodywork;
    public $handling;
    public $color;
    
    function __construct($pbrand, $pmodel, $pyear, $pdoors, $pprice, $pgas, $pmileage, $pbodywork, $phandling, $pcolor) {
        $this->brand = $pbrand;
        $this->model = $pmodel;
        $this->year = $pyear;
        $this->doors = $pdoors;
        $this->price = $pprice;
        $this->gas = $pgas;
        $this->mileage = $pmileage;
        $this->bodywork = $pbodywork;
        $this->handling = $phandling;
        $this->color = $pcolor;
    }      
}

class ExtraOption extends Car {
    public $options;
    
    function __construct($pbrand, $pmodel, $pyear, $pdoors, $pprice, $pgas, $pmileage, $pbodywork, $phandling, $pcolor, $poptions) {
        parent::__construct($pbrand, $pmodel, $pyear, $pdoors, $pprice, $pgas, $pmileage, $pbodywork, $phandling, $pcolor);
        $this->options = $poptions;
    }
}

$c1 = new Car('BMW', '320i', 2006, '5 doors', '&euro; 6.150,-', 'Gasoline', "145.000", 'Sedan', 'Manual', 'Black');
$c2 = new ExtraOption('BMW', '325i', 2006, '5 doors', '&euro; 8.250,-', 'Gasoline', "178.000", 'Stationwagon', 'Automatic', 'Navy Blue Metallic', 'Navigation, Climate control, Sport Seats, Half Leather, 17 inch Rims');
$c3 = new ExtraOption('BMW', '320d', 2008, '5 doors', '&euro; 7.600,-', 'Diesel', "244.000", 'Stationwagon', 'Automatic', 'Grey Metallic', 'Navigation, Sport Seats, Half leather');

$cars = array($c1, $c2, $c3);

foreach($cars as $car){
    echo "Brand: " . $car->brand . "<br>" . "Model: " . $car->model . "<br>" . "Year:  " . $car->year . "<br>" . "Doors: " . $car->doors . "<br>" . "Price: " . $car->price . "<br>" . "Gas: " . $car->gas . "<br>" . "Milages: " . $car->mileage . "<br>" . "Bodywork: " . $car->bodywork . "<br>" . "Handlin: " . $car->handling . "<br>" . "Color: " . $car->color . "<br>" ;
    if ($car instanceof ExtraOption) {
        echo  "Extra Options: " . $car->options . "<br>";
    }
    echo "<br>";
    }

?>